Change log
===

## v2.1.0 (2023-01-11)

* Allow installation on PHP 8.2.

* Remove Doctrine annotations mistakenly kept from the 1.x branch.

## v2.0.0 (2021-11-30)

* Allow installation on PHP 8.1.

* Extend `PagerWave\Exception\ExceptionInterface` from `Throwable`.

* Remove classes deprecated in the 1.x branch.

* Remove dependency on extension packages. These must now be installed
  explicitly.

* Rename `$fieldName` parameter of `PagerWave\QueryInterface::get()` to
  `$param`, matching the implementation in `PagerWave\Query`. Named arguments
  may now be used safely.

## v1.3.0 (2020-11-29)

* Allow installation on PHP 8.0.

* Split off adapters and integrations into their own packages:

    * `PagerWave\Adapter\DoctrineAdapter` has been moved to
      `PagerWave\Extension\DoctrineOrm\QueryBuilderAdapter`, part of the
      `pagerwave/doctrine-orm-extension` package.

    * `PagerWave\Adapter\DoctrineDbalAdapter` has been moved to
      `PagerWave\Extension\DoctrineDbal\QueryBuilderAdapter`, part of the
      `pagerwave/doctrine-dbal-extension` package.

    * `PagerWave\Adapter\DoctrineSelectableAdapter` has been moved to
      `PagerWave\Extension\DoctrineCollections\SelectableAdapter`, part of the
      `pagerwave/doctrine-collection-extension` package.

    * `PagerWave\QueryReader\SymfonyRequestQueryReader`,
      `PagerWave\QueryReader\SymfonyRequestStackQueryReader`,
      `PagerWave\UrlGenerator\SymfonyRequestStackUrlGenerator`, and
      `PagerWave\UrlGenerator\SymfonyRequestUrlGenerator` have been moved to
      `PagerWave\Integration\Symfony\RequestQueryReader`,
      `PagerWave\Integration\Symfony\RequestStackQueryReader`,
      `PagerWave\Integration\Symfony\RequestStackUrlGenerator`, and
      `PagerWave\Integration\Symfony\RequestUrlGenerator`, respectively, part of
      the `pagerwave/symfony-http-foundation-integration` package.

    Class aliases have been created for backward compatibility. These will be
    removed in version 2.0. Additionally, the individual packages will also no
    longer be included as dependencies of PagerWave; you must explicitly require
    them in your project.

## v1.2.3 (2020-10-10)

* Empty update to have the package published with the new GitLab repository URL.

## v1.2.2 (2020-08-08)

* Fix regression in UnionAdapter fix that resulted in NULL entries appearing
  in `UnionAdapter::getEntries()` output.

## v1.2.1 (2020-08-08)

* Fix missing next page for UnionAdapter if only entries of a single type were
  present.

* Remove old branch alias.

## v1.2.0 (2020-05-09)

* Add missing `@covers` annotation.

* Move non-core functionality to `PagerWave\Extension` namespace:

    * `PagerWave\EntryReader\DateTimeEntryReaderDecorator` has been moved to
      `PagerWave\Extension\DateTime\DateTimeEntryReaderDecorator`.

    * `PagerWave\Validator\ValidatingDefinitionInterface` has been moved to
      `PagerWave\Extension\Validator\ValidatingDefinitionInterface.`

    * `PagerWave\Validator\ValidatingQueryReaderDecorator` has been moved to
      `PagerWave\Extension\Validator\ValidatingQueryReaderDecorator`.

    Class aliases have been created for backwards compatibility. These will be
    removed in version 2.0.

## v1.1.1 (2020-05-02)

* Fix PHP < 7.4 breakage in UnionAdapter.

## v1.1.0 (2020-05-02)

* Add new DoctrineDbalAdapter for paginating Doctrine DBAL query builders.

* Allow the UnionAdapter to take zero adapters, in which case an empty result
  will be returned.

* Fix mapping of non-root fields in DoctrineAdapter.

## v1.0.0 (2020-04-25)

* Initial release
