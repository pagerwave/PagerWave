<?php

declare(strict_types=1);

namespace PagerWave\Tests;

use PagerWave\Definition;
use PHPUnit\Framework\TestCase;

/**
 * @covers \PagerWave\Definition
 * @covers \PagerWave\DefinitionGroupTrait
 */
class DefinitionTest extends TestCase
{
    public function testGettingFieldNames(): void
    {
        $definition = new Definition([
            'ranking' => true,
            'id' => false,
        ]);
        $this->assertSame(['ranking', 'id'], $definition->getFieldNames());
    }

    /**
     * @dataProvider provideDefinitionConstructorArgs
     */
    public function testGetFieldOrder(array $definition): void
    {
        $definition = new Definition($definition);
        $this->assertTrue($definition->isFieldDescending('ranking'));
        $this->assertFalse($definition->isFieldDescending('id'));
    }

    public function testConstructorThrowsOnInvalidDefinitionOrder(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        new Definition(['ranking' => 'farts']);
    }

    public function testGettingInvalidFieldOrderThrowsException(): void
    {
        $definition = new Definition(['id' => false]);

        $this->expectException(\OutOfBoundsException::class);
        $definition->isFieldDescending('farts');
    }

    public function testCanCreateNewInstanceWithGroup(): void
    {
        $definition = (new Definition(['id' => 'DESC']))->withGroup('other');

        $this->assertSame('other', $definition->getGroup());
    }

    public function provideDefinitionConstructorArgs(): iterable
    {
        foreach ([true, SORT_DESC, 'desc', 'DESC'] as $rankingOrder) {
            foreach ([false, SORT_ASC, 'asc', 'ASC'] as $idOrder) {
                yield [['ranking' => $rankingOrder, 'id' => $idOrder]];
            }
        }
    }
}
