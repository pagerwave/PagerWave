<?php

declare(strict_types=1);

namespace PagerWave\Tests;

use PagerWave\Query;
use PHPUnit\Framework\TestCase;

/**
 * @covers \PagerWave\Query
 */
class QueryTest extends TestCase
{
    public function testFilledQuery(): void
    {
        $query = new Query([
            'ranking' => 4,
            'id' => 5,
        ]);

        $this->assertTrue($query->isFilled());
        $this->assertSame(4, $query->get('ranking'));
        $this->assertSame(5, $query->get('id'));
        $this->assertSame(['ranking' => 4, 'id' => 5], $query->all());
    }

    public function testThrowsWhenGettingInvalidParam(): void
    {
        $query = new Query(['filled' => 1]);

        $this->expectException(\OutOfBoundsException::class);
        $query->get('emptied');
    }

    public function testUnfilledQueryReportsAsUnfilled(): void
    {
        $query = new Query();

        $this->assertFalse($query->isFilled());
    }

    public function testUnfilledQueryThrowsWhenGettingParam(): void
    {
        $this->expectException(\OutOfBoundsException::class);

        (new Query())->get('foo');
    }

    public function testUnfilledQueryThrowsWhenGettingAllParams(): void
    {
        $this->expectException(\BadMethodCallException::class);

        (new Query())->all();
    }
}
