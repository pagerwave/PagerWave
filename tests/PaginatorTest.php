<?php

declare(strict_types=1);

namespace PagerWave\Tests;

use PagerWave\Adapter\ArrayAdapter;
use PagerWave\EntryReader\SimpleEntryReader;
use PagerWave\Paginator;
use PagerWave\Query;
use PagerWave\QueryReader\QueryReaderInterface;
use PagerWave\Tests\Fixtures\Entity;
use PagerWave\Tests\Fixtures\EntityDefinition;
use PagerWave\UrlGenerator\NativeUrlGenerator;
use PHPUnit\Framework\TestCase;

/**
 * @covers \PagerWave\Paginator
 */
class PaginatorTest extends TestCase
{
    /**
     * @var QueryReaderInterface|\PHPUnit\Framework\MockObject\MockObject
     */
    private $queryReader;

    /**
     * @var Paginator
     */
    private $paginator;

    protected function setUp(): void
    {
        $this->queryReader = $this->createMock(QueryReaderInterface::class);
        $this->paginator = new Paginator(
            new SimpleEntryReader(),
            $this->queryReader,
            new NativeUrlGenerator()
        );
    }

    public function testPaginatorWithEmptyQueryAndNextPage(): void
    {
        $this->queryReader
            ->expects($this->once())
            ->method('getFromRequest')
            ->willReturn(new Query());

        $adapter = new ArrayAdapter([
            new Entity(12, 6),
            new Entity(11, 5),
            new Entity(10, 4),
            new Entity(9, 3),
            new Entity(8, 2),
        ]);

        $pager = $this->paginator->paginate($adapter, 4, new EntityDefinition());

        $this->assertSame([6, 5, 4, 3], array_column(iterator_to_array($pager), 'id'));
        $this->assertSame(
            ['ranking' => 8, 'id' => 2],
            $pager->getNextPageParams()
        );
    }

    public function testPaginatorWithQueryAndEmptyNextPage(): void
    {
        $this->queryReader
            ->expects($this->once())
            ->method('getFromRequest')
            ->willReturn(new Query([
                'ranking' => 5,
                'id' => 3,
            ]));

        $adapter = new ArrayAdapter([
            new Entity(5, 4),
            new Entity(4, 3),
        ]);
        $pager = $this->paginator->paginate($adapter, 4, new EntityDefinition());

        $this->assertSame([4, 3], array_column(iterator_to_array($pager), 'id'));
    }
}
