<?php

declare(strict_types=1);

namespace PagerWave\Tests\QueryReader;

use PagerWave\Exception\IncompleteQueryException;
use PagerWave\QueryReader\ArrayQueryReader;
use PagerWave\Tests\Fixtures\EntityDefinition;
use PHPUnit\Framework\TestCase;

/**
 * @covers \PagerWave\QueryReader\ArrayQueryReader
 */
class ArrayQueryReaderTest extends TestCase
{
    public function testCanReadQuery(): void
    {
        $reader = new ArrayQueryReader([
            'next' => [
                'ranking' => '4',
                'id' => '3',
            ],
        ]);

        $query = $reader->getFromRequest(new EntityDefinition());

        $this->assertTrue($query->isFilled());
        $this->assertSame('4', $query->get('ranking'));
        $this->assertSame('3', $query->get('id'));
    }

    public function testCanReadMissingQuery(): void
    {
        $reader = new ArrayQueryReader([
            'foo' => 'bar',
            'what' => 'ever',
        ]);

        $query = $reader->getFromRequest(new EntityDefinition());
        $this->assertFalse($query->isFilled());
    }

    public function testThrowsOnIncompleteQuery(): void
    {
        $reader = new ArrayQueryReader([
            'next' => [
                'ranking' => '4',
                // missing 'id'
            ],
        ]);

        $this->expectException(IncompleteQueryException::class);

        $reader->getFromRequest(new EntityDefinition());
    }
}
