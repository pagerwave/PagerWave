<?php

declare(strict_types=1);

namespace PagerWave\Tests\QueryReader;

use PagerWave\Exception\IncompleteQueryException;
use PagerWave\QueryReader\NativeQueryReader;
use PagerWave\Tests\Fixtures\EntityDefinition;
use PHPUnit\Framework\TestCase;

/**
 * @backupGlobals enabled
 * @covers \PagerWave\QueryReader\NativeQueryReader
 */
class NativeQueryReaderTest extends TestCase
{
    /**
     * @var NativeQueryReader
     */
    private $reader;

    protected function setUp(): void
    {
        $this->reader = new NativeQueryReader();
    }

    public function testCanReadQuery(): void
    {
        $_GET = [
            'next' => [
                'ranking' => '4',
                'id' => '3',
            ],
        ];

        $query = $this->reader->getFromRequest(new EntityDefinition());

        $this->assertTrue($query->isFilled());
        $this->assertSame('4', $query->get('ranking'));
        $this->assertSame('3', $query->get('id'));
    }

    public function testCanReadMissingQuery(): void
    {
        $_GET = [
            'foo' => 'bar',
            'what' => 'ever',
        ];

        $query = $this->reader->getFromRequest(new EntityDefinition());
        $this->assertFalse($query->isFilled());
    }

    public function testThrowsOnIncompleteQuery(): void
    {
        $_GET = [
            'next' => [
                'ranking' => '4',
                // missing 'id'
            ],
        ];

        $this->expectException(IncompleteQueryException::class);

        $this->reader->getFromRequest(new EntityDefinition());
    }
}
