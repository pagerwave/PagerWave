<?php

declare(strict_types=1);

namespace PagerWave\Tests\EntryReader;

use PagerWave\EntryReader\SimpleEntryReader;
use PagerWave\Tests\Fixtures\Entity;
use PHPUnit\Framework\TestCase;

/**
 * @covers \PagerWave\EntryReader\SimpleEntryReader
 */
class SimpleEntryReaderTest extends TestCase
{
    /**
     * @dataProvider provideValidEntries
     *
     * @param array|object $entry
     */
    public function testRead($entry): void
    {
        $reader = new SimpleEntryReader();
        $this->assertSame(4, $reader->read($entry, 'ranking'));
        $this->assertSame(3, $reader->read($entry, 'id'));
    }

    /**
     * @dataProvider provideInvalidEntries
     *
     * @param array|object $entry
     */
    public function testReadingInvalidEntriesReturnsNull($entry): void
    {
        $reader = new SimpleEntryReader();
        $this->assertNull($reader->read($entry, 'ranking'));
    }

    public function provideValidEntries(): iterable
    {
        yield [new Entity(4, 3)];
        yield [['ranking' => 4, 'id' => 3]];
        yield [(object) ['ranking' => 4, 'id' => 3]];
        yield [new \ArrayObject(['ranking' => 4, 'id' => 3])];
        yield [new class() {
            public $ranking = 4;
            public $id = 3;
        }];
        yield [new class() {
            public function __isset($name): bool
            {
                return \in_array($name, ['ranking', 'id'], true);
            }

            public function __get($name): int
            {
                return ['ranking' => 4, 'id' => 3][$name];
            }

            public function __set($name, $value): void
            {
            }
        }];
        yield [new class() {
            public function getRanking(): int
            {
                return 4;
            }

            public function getId(): int
            {
                return 3;
            }
        }];
    }

    public function provideInvalidEntries(): iterable
    {
        yield [null];
        yield [123];
        yield ['invalid'];
        yield [[]];
        yield [(object) []];
        yield [new class() {
            public function __isset($name): bool
            {
                return false;
            }

            public function __get($name): void
            {
                throw new \LogicException('uh');
            }

            public function __set($name, $value): void
            {
            }
        }];
    }
}
