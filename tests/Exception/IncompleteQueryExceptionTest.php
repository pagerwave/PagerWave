<?php

declare(strict_types=1);

namespace PagerWave\Tests\Exception;

use PagerWave\Exception\IncompleteQueryException;
use PagerWave\Tests\Fixtures\EntityDefinition;
use PHPUnit\Framework\TestCase;

/**
 * @covers \PagerWave\Exception\IncompleteQueryException
 */
class IncompleteQueryExceptionTest extends TestCase
{
    public function testExceptionMessage(): void
    {
        $e = IncompleteQueryException::create(new EntityDefinition(), ['id', 'foo']);

        $this->assertSame(
            'Missing keys in pagination query (needs ["ranking", "id"]; given ["id"])',
            $e->getMessage()
        );
    }
}
