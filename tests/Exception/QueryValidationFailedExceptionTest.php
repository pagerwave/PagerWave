<?php

declare(strict_types=1);

namespace PagerWave\Tests\Exception;

use PagerWave\Exception\QueryValidationFailedException;
use PHPUnit\Framework\TestCase;

/**
 * @covers \PagerWave\Exception\QueryValidationFailedException
 */
class QueryValidationFailedExceptionTest extends TestCase
{
    public function testExceptionMessage(): void
    {
        $message = QueryValidationFailedException::create('foo')->getMessage();

        $this->assertSame("Validation failed for parameter 'foo'", $message);
    }
}
