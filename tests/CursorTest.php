<?php

declare(strict_types=1);

namespace PagerWave\Tests;

use PagerWave\Cursor;
use PHPUnit\Framework\TestCase;

/**
 * @covers \PagerWave\Cursor
 */
class CursorTest extends TestCase
{
    public function testEmptyPager(): void
    {
        $cursor = new Cursor([], [], null);

        $this->assertCount(0, $cursor);
        $this->assertTrue($cursor->isEmpty());
        $this->assertSame([], \iterator_to_array($cursor));
        $this->assertFalse($cursor->hasNextPage());
    }

    public function testPopulatedPager(): void
    {
        $cursor = new Cursor([1, 2, 3], [4], 'http://the-url');

        $this->assertCount(3, $cursor);
        $this->assertFalse($cursor->isEmpty());
        $this->assertSame([1, 2, 3], \iterator_to_array($cursor));
        $this->assertTrue($cursor->hasNextPage());
        $this->assertSame('http://the-url', $cursor->getNextPageUrl());
        $this->assertSame([4], $cursor->getNextPageParams());
    }

    public function testGettingNextPageUrlThrowsIfThereIsNoNextPage(): void
    {
        $cursor = new Cursor([1, 2, 3], [], 'http://the-url');

        $this->expectException(\OutOfRangeException::class);
        $cursor->getNextPageUrl();
    }

    public function testGettingNextPageParamsThrowsIfThereIsNoNextPage(): void
    {
        $cursor = new Cursor([1, 2, 3], [], 'http://the-url');

        $this->expectException(\OutOfRangeException::class);
        $cursor->getNextPageParams();
    }

    public function testHasNoPreviousPage(): void
    {
        $cursor = new Cursor([1, 2, 3], [4], null);

        $this->assertFalse($cursor->hasPreviousPage());
    }

    public function testGettingPreviousPageUrlThrowsException(): void
    {
        $this->expectException(\OutOfRangeException::class);

        (new Cursor([1, 2, 3], [4], null))->getPreviousPageUrl();
    }

    public function testGettingPreviousPageParamsThrowsException(): void
    {
        $this->expectException(\OutOfRangeException::class);

        (new Cursor([1, 2, 3], [4], null))->getPreviousPageParams();
    }
}
