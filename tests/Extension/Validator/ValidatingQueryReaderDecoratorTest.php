<?php

declare(strict_types=1);

namespace PagerWave\Tests\Extension\Validator;

use PagerWave\Definition;
use PagerWave\Exception\QueryValidationFailedException;
use PagerWave\Extension\Validator\ValidatingQueryReaderDecorator;
use PagerWave\QueryReader\ArrayQueryReader;
use PagerWave\Tests\Fixtures\EntityDefinition;
use PHPUnit\Framework\TestCase;

/**
 * @covers \PagerWave\Extension\Validator\ValidatingQueryReaderDecorator
 */
class ValidatingQueryReaderDecoratorTest extends TestCase
{
    public function testReadsValidQuery(): void
    {
        $reader = new ValidatingQueryReaderDecorator(
            new ArrayQueryReader(['next' => ['ranking' => '4', 'id' => '3']])
        );

        $query = $reader->getFromRequest(new EntityDefinition());

        $this->assertSame('4', $query->get('ranking'));
        $this->assertSame('3', $query->get('id'));
    }

    public function testThrowsOnInvalidQuery(): void
    {
        $reader = new ValidatingQueryReaderDecorator(
            new ArrayQueryReader(['next' => ['ranking' => '4', 'id' => 'a']])
        );

        $this->expectException(QueryValidationFailedException::class);

        $reader->getFromRequest(new EntityDefinition());
    }

    public function testNonValidatingDefinitionPassesThrough(): void
    {
        $reader = new ValidatingQueryReaderDecorator(
            new ArrayQueryReader(['next' => ['foo' => 'bar']])
        );

        $query = $reader->getFromRequest(new Definition(['foo' => SORT_DESC]));

        $this->assertSame(['foo' => 'bar'], $query->all());
    }

    public function testUnfilledQueryPassesThrough(): void
    {
        $reader = new ValidatingQueryReaderDecorator(new ArrayQueryReader([]));

        $query = $reader->getFromRequest(new EntityDefinition());

        $this->assertFalse($query->isFilled());
    }
}
