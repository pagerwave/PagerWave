<?php

declare(strict_types=1);

namespace PagerWave\Tests\Extension\DateTime;

use PagerWave\EntryReader\SimpleEntryReader;
use PagerWave\Extension\DateTime\DateTimeEntryReaderDecorator;
use PHPUnit\Framework\TestCase;

/**
 * @covers \PagerWave\Extension\DateTime\DateTimeEntryReaderDecorator
 */
class DateTimeEntryReaderDecoratorTest extends TestCase
{
    /**
     * @var DateTimeEntryReaderDecorator
     */
    private $reader;

    protected function setUp(): void
    {
        $this->reader = new DateTimeEntryReaderDecorator(
            new SimpleEntryReader()
        );
    }

    /**
     * @dataProvider provideDates
     */
    public function testTransformsDateObjects(string $expected, \DateTimeInterface $date): void
    {
        $entry = ['date' => $date];

        $this->assertSame($expected, $this->reader->read($entry, 'date'));
    }

    /**
     * @dataProvider provideNonDates
     *
     * @param mixed $nonDate
     */
    public function testIgnoresNonDateObjects($nonDate): void
    {
        $entry = ['date' => $nonDate];

        $this->assertSame($nonDate, $this->reader->read($entry, 'date'));
    }

    public function provideDates(): iterable
    {
        yield [
            '2020-04-21T22:46:24+00:00',
            new \DateTime('2020-04-21T22:46:24+00:00'),
        ];

        yield [
            '2020-04-21T22:46:24+00:00',
            new \DateTimeImmutable('2020-04-21T22:46:24+00:00'),
        ];
    }

    public function provideNonDates(): iterable
    {
        yield ['foo'];
        yield ['2020-04-21T22:46:24+00:00'];
        yield [(object) []];
    }
}
