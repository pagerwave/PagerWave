<?php

declare(strict_types=1);

namespace PagerWave\Tests;

use PagerWave\AdapterResult;
use PagerWave\Tests\Fixtures\Entity;
use PHPUnit\Framework\TestCase;

/**
 * @covers \PagerWave\AdapterResult
 */
class AdapterResultTest extends TestCase
{
    public function testWithPopulatedResults(): void
    {
        $entries = [
            new Entity(4, 20),
            new Entity(4, 18),
            new Entity(6, 1),
            new Entity(1, 12),
        ];
        $nextEntry = new Entity(0, 12);

        $adapterResult = new AdapterResult($entries, $nextEntry);

        $this->assertSame($entries, $adapterResult->getEntries());
        $this->assertSame($nextEntry, $adapterResult->getNextEntry());
    }

    public function testWithEmptyResults(): void
    {
        $adapterResult = new AdapterResult([], null);

        $this->assertSame([], $adapterResult->getEntries());
        $this->assertNull($adapterResult->getNextEntry());
    }
}
