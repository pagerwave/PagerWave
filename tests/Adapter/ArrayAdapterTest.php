<?php

declare(strict_types=1);

namespace PagerWave\Tests\Adapter;

use PagerWave\Adapter\ArrayAdapter;
use PagerWave\Query;
use PagerWave\Tests\Fixtures\Entity;
use PagerWave\Tests\Fixtures\EntityDefinition;
use PHPUnit\Framework\TestCase;

/**
 * @covers \PagerWave\Adapter\ArrayAdapter
 */
class ArrayAdapterTest extends TestCase
{
    /**
     * @var ArrayAdapter
     */
    private $adapter;

    protected function setUp(): void
    {
        $this->adapter = new ArrayAdapter([
            new Entity(4, 2),
            new Entity(4, 3),
            new Entity(7, 4),
            new Entity(3, 11),
            new Entity(84, 6),
            new Entity(12, 12),
            new Entity(4, 1),
        ]);
    }

    public function testPaging(): void
    {
        $result = $this->adapter->getResults(5, new EntityDefinition(), new Query());

        $this->assertSame([6, 12, 4, 1, 2], array_column($result->getEntries(), 'id'));
        $this->assertSame(3, $result->getNextEntry()->id);
    }

    public function testPagingWithQuery(): void
    {
        $result = $this->adapter->getResults(5, new EntityDefinition(), new Query([
            'ranking' => 4,
            'id' => 3,
        ]));

        $this->assertSame([3, 11], array_column($result->getEntries(), 'id'));
        $this->assertNull($result->getNextEntry());
    }

    public function testPagingWithQueryAndFilteringSkipped(): void
    {
        $definition = new EntityDefinition();
        $query = new Query([
            'ranking' => 3,
            'id' => 2,
        ]);
        $result = $this->adapter
            ->withFilteringSkipped()
            ->getResults(2, $definition, $query);

        $this->assertSame([6, 12], array_column($result->getEntries(), 'id'));
    }
}
