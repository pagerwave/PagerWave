<?php

declare(strict_types=1);

namespace PagerWave\Tests\Adapter;

use PagerWave\Adapter\ArrayAdapter;
use PagerWave\Adapter\UnionAdapter;
use PagerWave\Query;
use PagerWave\Tests\Fixtures\Entity;
use PagerWave\Tests\Fixtures\EntityDefinition;
use PHPUnit\Framework\TestCase;

/**
 * @covers \PagerWave\Adapter\UnionAdapter
 */
class UnionAdapterTest extends TestCase
{
    public function testPaging(): void
    {
        $adapter = new UnionAdapter(
            new ArrayAdapter([
                new Entity(4, 2),
                new Entity(4, 3),
                new Entity(7, 4),
            ]),
            new ArrayAdapter([
                new Entity(4, 5),
                new Entity(10, 1),
                new Entity(6, 6),
                new Entity(2, 100),
            ])
        );

        $adapterResult = $adapter->getResults(5, new EntityDefinition(), new Query());
        $entries = $adapterResult->getEntries();

        $this->assertSame([1, 4, 6, 2, 3], array_column($entries, 'id'));
        $this->assertSame(5, $adapterResult->getNextEntry()->id);
    }

    public function testGivesEmptyResultOnNoAdapter(): void
    {
        $results = (new UnionAdapter())->getResults(25, new EntityDefinition(), new Query());

        $this->assertSame([], $results->getEntries());
        $this->assertNull($results->getNextEntry());
    }

    /**
     * @see https://gitlab.com/postmill/PagerWave/-/issues/1
     */
    public function testHasNextPageIfOnlyOneAdapterProvidedEntries(): void
    {
        $adapter = new UnionAdapter(
            new ArrayAdapter([
                new Entity(1, 2),
                new Entity(1, 3),
                new Entity(1, 4),
            ]),
            new ArrayAdapter([])
        );

        $adapterResult = $adapter->getResults(2, new EntityDefinition(), new Query());
        $entries = $adapterResult->getEntries();
        $next = $adapterResult->getNextEntry();

        $this->assertSame([2, 3], array_column($entries, 'id'));
        $this->assertSame(4, $next->id);
    }

    /**
     * @see https://gitlab.com/postmill/PagerWave/-/issues/2
     */
    public function testPaginatingWithNoNextPage(): void
    {
        $adapter = new UnionAdapter(
            new ArrayAdapter([
                new Entity(1, 2),
                new Entity(1, 3),
                new Entity(1, 4),
            ])
        );

        $adapterResult = $adapter->getResults(3, new EntityDefinition(), new Query());
        $entries = $adapterResult->getEntries();

        $this->assertSame([2, 3, 4], array_column($entries, 'id'));
        $this->assertNull($adapterResult->getNextEntry());
    }
}
