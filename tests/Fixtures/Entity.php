<?php

declare(strict_types=1);

namespace PagerWave\Tests\Fixtures;

class Entity
{
    public $ranking;

    public $id;

    public function __construct(int $ranking, int $id)
    {
        $this->ranking = $ranking;
        $this->id = $id;
    }
}
