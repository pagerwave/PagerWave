<?php

declare(strict_types=1);

namespace PagerWave\Tests\Fixtures;

use PagerWave\DefinitionGroupTrait;
use PagerWave\DefinitionInterface;
use PagerWave\Extension\Validator\ValidatingDefinitionInterface;

class EntityDefinition implements DefinitionInterface, ValidatingDefinitionInterface
{
    use DefinitionGroupTrait;

    public function getFieldNames(): array
    {
        return ['ranking', 'id'];
    }

    public function isFieldDescending(string $fieldName): bool
    {
        return $fieldName === 'ranking';
    }

    public function isFieldValid(string $fieldName, $value): bool
    {
        return \is_numeric($value);
    }
}
