<?php

declare(strict_types=1);

namespace PagerWave\Tests\UrlGenerator;

use PagerWave\UrlGenerator\NativeUrlGenerator;
use PHPUnit\Framework\TestCase;

/**
 * @covers \PagerWave\UrlGenerator\NativeUrlGenerator
 */
class NativeUrlGeneratorTest extends TestCase
{
    /**
     * @backupGlobals enabled
     * @dataProvider provideEnvironment
     */
    public function testCanGenerateUrl(string $expected, array $params, array $server, array $get = []): void
    {
        $_GET = array_replace($_GET, $get);
        $_SERVER = array_replace($_SERVER, $server);

        $generated = (new NativeUrlGenerator())->generateUrl('next', $params);

        $this->assertSame($expected, $generated);
    }

    public function provideEnvironment(): iterable
    {
        yield [
            'http://localhost/?next%5Branking%5D=4&next%5Bid%5D=3',
            ['ranking' => 4, 'id' => 3],
            ['HTTP_HOST' => 'localhost', 'REQUEST_URI' => '/', 'SERVER_PORT' => '80'],
        ];
        yield [
            'http://localhost/?foo=bar&next%5Branking%5D=4&next%5Bid%5D=3',
            ['ranking' => 4, 'id' => 3],
            ['HTTP_HOST' => 'localhost', 'REQUEST_URI' => '/', 'SERVER_PORT' => '80'],
            ['foo' => 'bar'],
        ];
        yield [
            'https://localhost/?next%5Branking%5D=4&next%5Bid%5D=3',
            ['ranking' => 4, 'id' => 3],
            ['HTTP_HOST' => 'localhost', 'REQUEST_URI' => '/', 'HTTPS' => '1', 'SERVER_PORT' => '443'],
        ];
        yield [
            'http://127.0.0.1/foo?next%5Branking%5D=4&next%5Bid%5D=3',
            ['ranking' => 4, 'id' => 3],
            ['REQUEST_URI' => '/foo', 'SERVER_ADDR' => '127.0.0.1'],
        ];
        yield [
            'http://localhost:443/?foo%5B0%5D=bar&foo%5B1%5D=baz',
            [],
            ['HTTP_HOST' => 'localhost', 'REQUEST_URI' => '/', 'SERVER_PORT' => '443'],
            ['foo' => ['bar', 'baz']],
        ];
        yield [
            'https://[::1]:8080/foo/bar?next%5Branking%5D=4&next%5Bid%5D=3',
            ['ranking' => 4, 'id' => 3],
            ['REQUEST_URI' => '/foo/bar', 'HTTPS' => 1, 'SERVER_ADDR' => '::1', 'SERVER_PORT' => '8080', 'PHP_AUTH_USER' => 'emma', 'PHP_AUTH_PW' => 'pw'],
        ];
    }
}
