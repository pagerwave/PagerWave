Transforming entry & query values
===

Sometimes data needs to be of a different type, or otherwise be transformed
prior to paginating or generating URLs for your pages. PagerWave allows this
through the use of the [decorator pattern][1].

When constructing a `PagerWave\Paginator` object, you can provide decorated
`EntryReaderInterface` and `QueryReaderInterface` objects that transform
requested values before the adapters or URL generator sees them.

[1]: https://en.wikipedia.org/wiki/Decorator_pattern

Transforming DateTime objects
---

A common need in pagination is to sort on fields that are `DateTime` or
`DateTimeImmutable` instances. Unfortunately, PHP's date objects do not
implement the `__toString()` method, and so URL generation will fail.

To solve this, PagerWave includes a decorator for `EntryReaderInterface` that
converts date objects to ISO-8601-formatted strings.

~~~php
$paginator = new \PagerWave\Paginator(
    new \PagerWave\Extension\DateTime\DateTimeEntryReaderDecorator(
        new \PagerWave\EntryReader\SimpleEntryReader()
    ),
    new \PagerWave\QueryReader\NativeQueryReader(),
    new \PagerWave\UrlGenerator\NativeUrlGenerator()
);
~~~

The reverse operation, transforming date strings to objects, tends to be
unnecessary, as ISO 8601 strings are usually supported where a date object is
accepted. For instance, Doctrine ORM works fine when binding date strings to
timestamp columns. Such a transformer is therefore not included.
