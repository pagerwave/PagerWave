# Using multiple cursors on a single page

You can use multiple cursors on a single page by using the `withGroup()` method
on your definition object:

~~~php
use PagerWave\Extension\DoctrineOrm\QueryBuilderAdapter;
use PagerWave\Definition;

$articleAdapter = new QueryBuilderAdapter(getArticleBuilder());
$commentAdapter = new QueryBuilderAdapter(getCommentBuilder());

// Be aware that `withGroup()` return a cloned object - it is not a setter.
$articleDefinition = (new Definition(['id' => 'desc']))->withGroup('t');
$commentDefinition = (new Definition(['id' => 'desc']))->withGroup('c');

$articles = $paginator->paginate($articleAdapter, 25, $articleDefinition);
$comments = $paginator->paginate($commentAdapter, 25, $commentDefinition);

echo $articles->getNextPageUrl();
~~~

The URLs will now contain existing parameters, if any, for comments, and the
parameters for the next page for articles.
