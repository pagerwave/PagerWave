Defining sort order
===

To PagerWave, knowing how to sort entries is crucial. For this, it needs the
following:

1. Which fields to sort by
2. Which direction to order each field by

It is the job of a `DefinitionInterface` instance to provide this information.

A pre-made implementation of this interface is `PagerWave\Definition`. It is
created with a single array argument that maps field names to their respective
sort directions:

~~~php
use PagerWave\Definition;

$definition = new Definition([
    // These are all equivalent:
    'timestamp' => SORT_DESC,
    'timestamp' => 'DESC', // case-insensitive
    'timestamp' => true,

    // So are these:
    'id' => SORT_ASC,
    'id' => 'ASC', // case-insensitive
    'id' => false,
]);
~~~

(If you weren't already aware, arrays in PHP are ordered hashmaps.)

This definition tells PagerWave to order entries by their timestamps, and if two
or more entries have the same timestamp, resolve this ambiguity by ordering by
ID.
