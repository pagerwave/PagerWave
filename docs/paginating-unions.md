Paginating results from multiple collections
===

PagerWave lets you paginate the combined results from multiple collections using
the [UnionAdapter](../src/Adapter/UnionAdapter.php).

~~~php
use PagerWave\Adapter\UnionAdapter;
use PagerWave\Extension\DoctrineOrm\QueryBuilderAdapter;
use PagerWave\Definition;

$articleQb = $em->createQueryBuilder()->select('a')->from(Article::class, 'a');
$commentQb = $em->createQueryBuilder()->select('c')->from(Comment::class, 'c');

$unionAdapter = new UnionAdapter(
    new QueryBuilderAdapter($articleQb),
    new QueryBuilderAdapter($commentQb)
);

$combinedResults = $paginator->paginate($unionAdapter, 50, new Definition([
    'timestamp' => SORT_DESC,
]));

foreach ($combinedResults as $result) {
    // $result is either an article or a comment
}
~~~

The prerequisites for making this work are...

1. ... that your adapters have all the fields in common.

   Timestamps are the best way to accomplish this.

2. ... that every field is sequential between each adapter.

   For instance, if the 'articles' and 'comments' in the example above each have
   their own auto-incrementing ID sequences, you could not sort by ID without
   getting incorrect results.

You are free to mix and match different types of adapters as long as these
requirements are met.
