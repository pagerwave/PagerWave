# PagerWave docs

1. [Introduction](../README.md)
2. [Defining sort order](defining-sort-order.md)
3. [Paginating results from multiple collections (the "union adapter")](paginating-unions.md)
4. [Multiple cursors on a single page](multiple-cursors-on-single-page.md)
5. [Using transformers](using-transformers.md)
    1. [Transforming DateTime objects](using-transformers.md#transforming-datetime-objects)
