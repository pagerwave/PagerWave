<?php

declare(strict_types=1);

namespace PagerWave;

final class Query implements QueryInterface
{
    /**
     * @var array
     */
    private $params;

    /**
     * @param array a map of field names and their corresponding values, in
     *              definition order
     */
    public function __construct(array $params = [])
    {
        $this->params = $params;
    }

    public function isFilled(): bool
    {
        return \count($this->params) > 0;
    }

    public function get(string $param)
    {
        if (!isset($this->params[$param])) {
            throw new \OutOfBoundsException("Parameter '$param' does not exist");
        }

        return $this->params[$param];
    }

    public function all(): array
    {
        if (\count($this->params) === 0) {
            throw new \BadMethodCallException('The query wasn\'t filled');
        }

        return $this->params;
    }
}
