<?php

declare(strict_types=1);

namespace PagerWave\QueryReader;

use PagerWave\DefinitionInterface;
use PagerWave\Exception\IncompleteQueryException;
use PagerWave\Exception\InvalidQueryException;
use PagerWave\Exception\QueryValidationFailedException;
use PagerWave\QueryInterface;

interface QueryReaderInterface
{
    /**
     * Given a definition, creates a Query object based on parameters from the
     * current request.
     *
     * The query created must have its parameters in definition order.
     *
     * If the query is not defined in the request, a `QueryInterface` where
     * `isFilled()` returns false must be returned.
     *
     * @throws IncompleteQueryException       if the query is defined in the request, but is incomplete as per the
     *                                        definition
     * @throws QueryValidationFailedException if the query has all the field names, but is invalid
     * @throws InvalidQueryException          on other faults with the query
     */
    public function getFromRequest(DefinitionInterface $definition): QueryInterface;
}
