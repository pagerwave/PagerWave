<?php

declare(strict_types=1);

namespace PagerWave\QueryReader;

use PagerWave\DefinitionInterface;
use PagerWave\Exception\IncompleteQueryException;
use PagerWave\Query;
use PagerWave\QueryInterface;

final class ArrayQueryReader implements QueryReaderInterface
{
    /**
     * @var array
     */
    private $params;

    public function __construct(array $params)
    {
        $this->params = $params;
    }

    public function getFromRequest(DefinitionInterface $definition): QueryInterface
    {
        $next = $this->params[$definition->getGroup()] ?? [];

        if ($next) {
            foreach ($definition->getFieldNames() as $fieldName) {
                if (!isset($next[$fieldName])) {
                    throw IncompleteQueryException::create($definition, \array_keys($next));
                }

                $params[$fieldName] = $next[$fieldName];
            }
        }

        return new Query($params ?? []);
    }
}
