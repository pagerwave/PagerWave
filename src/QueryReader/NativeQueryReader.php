<?php

declare(strict_types=1);

namespace PagerWave\QueryReader;

use PagerWave\DefinitionInterface;
use PagerWave\QueryInterface;

/**
 * Reads query parameters from PHP's `$_GET` superglobal.
 */
final class NativeQueryReader implements QueryReaderInterface
{
    public function getFromRequest(DefinitionInterface $definition): QueryInterface
    {
        return (new ArrayQueryReader($_GET))->getFromRequest($definition);
    }
}
