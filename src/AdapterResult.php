<?php

declare(strict_types=1);

namespace PagerWave;

final class AdapterResult implements AdapterResultInterface
{
    /**
     * @var array
     */
    private $entries;

    /**
     * @var object|mixed|null
     */
    private $nextEntry;

    public function __construct(array $entries, $nextEntry)
    {
        $this->entries = $entries;
        $this->nextEntry = $nextEntry;
    }

    public function getEntries(): array
    {
        return $this->entries;
    }

    public function getNextEntry()
    {
        return $this->nextEntry;
    }
}
