<?php

declare(strict_types=1);

namespace PagerWave;

use PagerWave\Adapter\AdapterInterface;
use PagerWave\Exception\IncompleteQueryException;

interface PaginatorInterface
{
    /**
     * @throws IncompleteQueryException if request params are present, but
     *                                  incomplete
     */
    public function paginate(
        AdapterInterface $adapter,
        int $max,
        DefinitionInterface $definition
    ): CursorInterface;
}
