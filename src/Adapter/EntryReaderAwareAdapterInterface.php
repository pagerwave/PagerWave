<?php

declare(strict_types=1);

namespace PagerWave\Adapter;

use PagerWave\EntryReader\EntryReaderInterface;

interface EntryReaderAwareAdapterInterface extends AdapterInterface
{
    /**
     * Create a new instance of the adapter with the specified entry reader.
     *
     * @noinspection PhpMissingReturnTypeInspection
     *
     * @return static
     */
    public function withEntryReader(EntryReaderInterface $entryReader);
}
