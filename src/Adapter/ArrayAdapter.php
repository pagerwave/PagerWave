<?php

declare(strict_types=1);

namespace PagerWave\Adapter;

use PagerWave\AdapterResult;
use PagerWave\AdapterResultInterface;
use PagerWave\DefinitionInterface;
use PagerWave\EntryReader\EntryReaderInterface;
use PagerWave\EntryReader\SimpleEntryReader;
use PagerWave\QueryInterface;

/**
 * Paginates an array of entries.
 *
 * Each query parameter must be comparable using PHP's comparison operators.
 *
 * If the entries are known not to contain entries that would be displayed on
 * any previous pages, an instance created with `withFilteringSkipped()` may be
 * used for a performance boost.
 */
final class ArrayAdapter implements EntryReaderAwareAdapterInterface
{
    /**
     * @var array
     */
    private $entries;

    /**
     * @var EntryReaderInterface
     */
    private $entryReader;

    private $filteringSkipped = false;

    public function __construct(array $entries)
    {
        $this->entries = $entries;
        $this->entryReader = new SimpleEntryReader();
    }

    public function getResults(
        int $max,
        DefinitionInterface $definition,
        QueryInterface $query
    ): AdapterResultInterface {
        $entries = $this->entries;
        $sortMap = array_combine(
            $definition->getFieldNames(),
            array_map(
                [$definition, 'isFieldDescending'],
                $definition->getFieldNames()
            )
        );

        if (!$this->filteringSkipped && $query->isFilled()) {
            $entries = \array_filter($entries, function ($entry) use ($query, $sortMap): bool {
                foreach ($query->all() as $field => $qv) {
                    $ev = $this->entryReader->read($entry, $field);

                    if ($sortMap[$field] ? $ev > $qv : $ev < $qv) {
                        return false;
                    }
                }

                return true;
            });
        }

        \usort($entries, function ($a, $b) use ($sortMap): int {
            foreach ($sortMap as $fieldName => $desc) {
                $av = $this->entryReader->read($a, $fieldName);
                $bv = $this->entryReader->read($b, $fieldName);
                $cmp = ($av <=> $bv) * ($desc ? -1 : 1);

                if ($cmp !== 0) {
                    break;
                }
            }

            return $cmp ?? 0;
        });

        $entries = \array_slice($entries, 0, $max + 1);
        $nextEntry = isset($entries[$max]) ? \array_pop($entries) : null;

        return new AdapterResult($entries, $nextEntry);
    }

    public function withFilteringSkipped(): self
    {
        $self = clone $this;
        $self->filteringSkipped = true;

        return $self;
    }

    public function withEntryReader(EntryReaderInterface $entryReader): self
    {
        $self = clone $this;
        $self->entryReader = $entryReader;

        return $self;
    }
}
