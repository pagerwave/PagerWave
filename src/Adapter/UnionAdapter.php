<?php

declare(strict_types=1);

namespace PagerWave\Adapter;

use PagerWave\AdapterResultInterface;
use PagerWave\DefinitionInterface;
use PagerWave\EntryReader\EntryReaderInterface;
use PagerWave\EntryReader\SimpleEntryReader;
use PagerWave\QueryInterface;

/**
 * Paginates the results of multiple adapters.
 */
final class UnionAdapter implements EntryReaderAwareAdapterInterface
{
    /**
     * @var AdapterInterface[]
     */
    private $adapters;

    /**
     * @var EntryReaderInterface
     */
    private $entryReader;

    public function __construct(AdapterInterface ...$adapters)
    {
        $this->adapters = $adapters;
        $this->entryReader = new SimpleEntryReader();
    }

    public function getResults(
        int $max,
        DefinitionInterface $definition,
        QueryInterface $query
    ): AdapterResultInterface {
        foreach ($this->adapters as $adapter) {
            $adapterResult = $adapter->getResults($max, $definition, $query);
            $results[] = $adapterResult->getEntries();
            $nextEntry = $adapterResult->getNextEntry();

            if ($nextEntry) {
                $results[] = [$adapterResult->getNextEntry()];
            }
        }

        $results = array_merge(...($results ?? [[]]));

        return (new ArrayAdapter($results))
            ->withEntryReader($this->entryReader)
            ->withFilteringSkipped()
            ->getResults($max, $definition, $query);
    }

    public function withEntryReader(EntryReaderInterface $entryReader): self
    {
        $self = clone $this;
        $self->entryReader = $entryReader;

        return $self;
    }
}
