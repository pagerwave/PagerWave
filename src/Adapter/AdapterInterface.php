<?php

declare(strict_types=1);

namespace PagerWave\Adapter;

use PagerWave\AdapterResultInterface;
use PagerWave\DefinitionInterface;
use PagerWave\QueryInterface;

interface AdapterInterface
{
    public function getResults(
        int $max,
        DefinitionInterface $definition,
        QueryInterface $query
    ): AdapterResultInterface;
}
