<?php

declare(strict_types=1);

namespace PagerWave;

use PagerWave\Adapter\AdapterInterface;
use PagerWave\Adapter\EntryReaderAwareAdapterInterface;
use PagerWave\EntryReader\EntryReaderInterface;
use PagerWave\QueryReader\QueryReaderInterface;
use PagerWave\UrlGenerator\UrlGeneratorInterface;

final class Paginator implements PaginatorInterface
{
    /**
     * @var EntryReaderInterface
     */
    private $modelEntryReader;

    /**
     * @var QueryReaderInterface
     */
    private $queryReader;

    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    /**
     * @var EntryReaderInterface
     */
    private $viewEntryReader;

    /**
     * The primary point of entry for paginating your stuff.
     */
    public function __construct(
        EntryReaderInterface $viewEntryReader,
        QueryReaderInterface $queryReader,
        UrlGeneratorInterface $urlGenerator,
        EntryReaderInterface $modelEntryReader = null
    ) {
        $this->viewEntryReader = $viewEntryReader;
        $this->queryReader = $queryReader;
        $this->urlGenerator = $urlGenerator;
        $this->modelEntryReader = $modelEntryReader ?? $viewEntryReader;
    }

    public function paginate(
        AdapterInterface $adapter,
        int $max,
        DefinitionInterface $definition
    ): CursorInterface {
        $query = $this->queryReader->getFromRequest($definition);

        if ($adapter instanceof EntryReaderAwareAdapterInterface) {
            $adapter = $adapter->withEntryReader($this->modelEntryReader);
        }

        $results = $adapter->getResults($max, $definition, $query);
        $next = $results->getNextEntry();

        if ($next) {
            foreach ($definition->getFieldNames() as $fieldName) {
                $nextParams[$fieldName] =
                    $this->viewEntryReader->read($next, $fieldName);
            }
        }

        return new Cursor(
            $results->getEntries(),
            $nextParams ?? [],
            $this->urlGenerator->generateUrl(
                $definition->getGroup(),
                $nextParams ?? []
            )
        );
    }
}
