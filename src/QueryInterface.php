<?php

declare(strict_types=1);

namespace PagerWave;

interface QueryInterface
{
    /**
     * @return bool if true, a complete set of valid query parameters for the
     *              corresponding definition exists
     */
    public function isFilled(): bool;

    /**
     * @return mixed the value of the query parameter
     *
     * @throws \OutOfBoundsException if field doesn't exist
     */
    public function get(string $param);

    /**
     * @return array field->value map of all parameters, in definition order
     *
     * @throws \BadMethodCallException if query isn't filled
     */
    public function all(): array;
}
