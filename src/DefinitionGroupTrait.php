<?php

declare(strict_types=1);

namespace PagerWave;

trait DefinitionGroupTrait
{
    /**
     * @var string
     */
    private $group = 'next';

    public function getGroup(): string
    {
        return $this->group;
    }

    /**
     * @return static
     */
    public function withGroup(string $group): self
    {
        $self = clone $this;
        $self->group = $group;

        return $self;
    }
}
