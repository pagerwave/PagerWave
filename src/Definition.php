<?php

declare(strict_types=1);

namespace PagerWave;

/**
 * Provides a quick way of providing a definition on the fly.
 */
final class Definition implements DefinitionInterface
{
    use DefinitionGroupTrait;

    /**
     * @var bool[]
     */
    private $definition = [];

    /**
     * Takes an *ordered* map of fields and their corresponding order (ascending
     * or descending).
     *
     * `false`, `"ASC"`, and `SORT_ASC` are all equivalent to ascending order.
     *
     * `true`, `"DESC"`, and `SORT_DESC` are all equivalent to descending order.
     *
     * The first fields take precedence when ordering.
     *
     * @param int[]|string[]|bool[] $definition
     */
    public function __construct(array $definition)
    {
        foreach ($definition as $fieldName => $order) {
            if ($order === SORT_ASC || (
                \is_string($order) && \strtoupper($order) === 'ASC'
            )) {
                $order = false;
            } elseif ($order === SORT_DESC || (
                \is_string($order) && \strtoupper($order) === 'DESC'
            )) {
                $order = true;
            }

            if (!\is_bool($order)) {
                throw new \InvalidArgumentException(\sprintf(
                    'Invalid order given for field "%s" (%s given)',
                    $fieldName,
                    \is_scalar($order) ? \var_export($order, true) : \gettype($order)
                ));
            }

            $this->definition[$fieldName] = $order;
        }
    }

    public function getFieldNames(): array
    {
        return \array_keys($this->definition);
    }

    public function isFieldDescending(string $fieldName): bool
    {
        if (!isset($this->definition[$fieldName])) {
            throw new \OutOfBoundsException('No such field');
        }

        return $this->definition[$fieldName];
    }
}
