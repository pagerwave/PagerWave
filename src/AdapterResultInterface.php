<?php

declare(strict_types=1);

namespace PagerWave;

/**
 * Intermediary representation of pagination results between adapter and cursor.
 */
interface AdapterResultInterface
{
    public function getEntries(): array;

    /**
     * Get the entry that is the first on the next page, if any.
     *
     * @return mixed|null
     */
    public function getNextEntry();
}
