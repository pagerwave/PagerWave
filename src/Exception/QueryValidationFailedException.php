<?php

declare(strict_types=1);

namespace PagerWave\Exception;

class QueryValidationFailedException extends InvalidQueryException
{
    public static function create(string $fieldName): self
    {
        return new self("Validation failed for parameter '$fieldName'");
    }
}
