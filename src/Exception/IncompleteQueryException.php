<?php

declare(strict_types=1);

namespace PagerWave\Exception;

use PagerWave\DefinitionInterface;

class IncompleteQueryException extends InvalidQueryException
{
    public static function create(DefinitionInterface $definition, array $availableKeys): self
    {
        $availableKeys = array_intersect($availableKeys, $definition->getFieldNames());

        return new self(sprintf(
            'Missing keys in pagination query (needs ["%s"]; given [%s])',
            implode('", "', $definition->getFieldNames()),
            $availableKeys ? '"'.implode('", "', $availableKeys).'"' : ''
        ));
    }
}
