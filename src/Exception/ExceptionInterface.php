<?php

declare(strict_types=1);

namespace PagerWave\Exception;

/**
 * Marker interface for exceptions thrown by this library.
 */
interface ExceptionInterface extends \Throwable
{
}
