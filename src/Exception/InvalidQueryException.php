<?php

declare(strict_types=1);

namespace PagerWave\Exception;

class InvalidQueryException extends \RuntimeException implements ExceptionInterface
{
}
