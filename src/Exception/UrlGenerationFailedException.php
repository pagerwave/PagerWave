<?php

declare(strict_types=1);

namespace PagerWave\Exception;

class UrlGenerationFailedException extends \RuntimeException implements ExceptionInterface
{
}
