<?php

declare(strict_types=1);

namespace PagerWave;

final class Cursor implements CursorInterface
{
    /**
     * @var array
     */
    private $entries;

    /**
     * @var array
     */
    private $nextPageParams;

    /**
     * @var string|null
     */
    private $nextPageUrl;

    public function __construct(
        array $entries,
        array $nextPageParams,
        ?string $nextPageUrl
    ) {
        $this->entries = $entries;
        $this->nextPageParams = $nextPageParams;
        $this->nextPageUrl = $nextPageUrl;
    }

    public function getIterator(): \Iterator
    {
        return new \ArrayIterator($this->entries);
    }

    public function hasNextPage(): bool
    {
        return !empty($this->nextPageParams);
    }

    public function getNextPageUrl(): string
    {
        if (!$this->hasNextPage()) {
            throw new \OutOfRangeException('There is no next page');
        }

        return $this->nextPageUrl;
    }

    /**
     * @throws \OutOfRangeException if there is no next page
     */
    public function getNextPageParams(): array
    {
        if (!$this->hasNextPage()) {
            throw new \OutOfRangeException('There is no next page');
        }

        return $this->nextPageParams;
    }

    public function hasPreviousPage(): bool
    {
        return false;
    }

    public function getPreviousPageUrl(): string
    {
        throw new \OutOfRangeException('This cursor does not support backwards navigation');
    }

    public function getPreviousPageParams(): array
    {
        throw new \OutOfRangeException('This cursor does not support backwards navigation');
    }

    public function isEmpty(): bool
    {
        return empty($this->entries);
    }

    public function count(): int
    {
        return \count($this->entries);
    }
}
