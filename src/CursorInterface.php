<?php

declare(strict_types=1);

namespace PagerWave;

use PagerWave\Exception\UrlGenerationFailedException;

interface CursorInterface extends \Countable, \IteratorAggregate
{
    public function hasNextPage(): bool;

    /**
     * @throws \OutOfRangeException         if there is no next page
     * @throws UrlGenerationFailedException if a URL cannot be generated
     */
    public function getNextPageUrl(): string;

    /**
     * @throws \OutOfRangeException if there is no next page
     */
    public function getNextPageParams(): array;

    public function hasPreviousPage(): bool;

    /**
     * @throws \OutOfRangeException         if there is no previous page
     * @throws UrlGenerationFailedException if a URL cannot be generated
     */
    public function getPreviousPageUrl(): string;

    /**
     * @throws \OutOfRangeException if there is no previous page
     */
    public function getPreviousPageParams(): array;

    public function isEmpty(): bool;
}
