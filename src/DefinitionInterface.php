<?php

declare(strict_types=1);

namespace PagerWave;

interface DefinitionInterface
{
    /**
     * @return string[]|int[]
     */
    public function getFieldNames(): array;

    /**
     * @throws \OutOfBoundsException if field does not exist
     */
    public function isFieldDescending(string $fieldName): bool;

    public function getGroup(): string;

    /**
     * @noinspection PhpMissingReturnTypeInspection
     *
     * @return static
     */
    public function withGroup(string $group);
}
