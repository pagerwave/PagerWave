<?php

declare(strict_types=1);

namespace PagerWave\EntryReader;

interface EntryReaderInterface
{
    /**
     * Reads parameters from an entry being paginated.
     *
     * The desired characteristics of the return value are dependent on the
     * context. For instance, when used in the `ArrayAdapter`, the reader
     * expects every value to not be NULL, and to be comparable using the `<=>`
     * operator.
     *
     * @param array|object|mixed $entry
     *
     * @return mixed|null
     */
    public function read($entry, string $fieldName);
}
