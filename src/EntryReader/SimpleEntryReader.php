<?php

declare(strict_types=1);

namespace PagerWave\EntryReader;

final class SimpleEntryReader implements EntryReaderInterface
{
    public function read($entry, string $fieldName)
    {
        if (
            (\is_array($entry) || $entry instanceof \ArrayAccess) &&
            isset($entry[$fieldName])
        ) {
            return $entry[$fieldName];
        }

        if (\is_object($entry)) {
            if (isset($entry->$fieldName)) {
                return $entry->$fieldName;
            }

            if (\is_callable([$entry, 'get'.$fieldName])) {
                return $entry->{'get'.$fieldName}();
            }
        }

        return null;
    }
}
