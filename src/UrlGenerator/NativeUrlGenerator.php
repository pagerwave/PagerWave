<?php

declare(strict_types=1);

namespace PagerWave\UrlGenerator;

final class NativeUrlGenerator implements UrlGeneratorInterface
{
    public function generateUrl(string $group, array $params): string
    {
        $https = ($_SERVER['HTTPS'] ?? false);
        $url = $https ? 'https' : 'http';

        $url .= '://';

        if (($_SERVER['HTTP_HOST'] ?? '') !== '') {
            $url .= $_SERVER['HTTP_HOST'];
        } elseif (strpos($_SERVER['SERVER_ADDR'] ?? '', ':') !== false) {
            $url .= '['.$_SERVER['SERVER_ADDR'].']';
        } else {
            $url .= $_SERVER['SERVER_ADDR'] ?? 'localhost';
        }

        if (
            ($https && (int) ($_SERVER['SERVER_PORT'] ?? 443) !== 443) ||
            (!$https && (int) ($_SERVER['SERVER_PORT'] ?? 80) !== 80)
        ) {
            $url .= ':';
            $url .= $_SERVER['SERVER_PORT'];
        }

        $url .= explode('?', $_SERVER['REQUEST_URI'] ?? '', 2)[0];

        $query = $_GET;

        if ($params) {
            $query[$group] = $params;
        } else {
            unset($query[$group]);
        }

        if ($query) {
            $url .= '?';
            $url .= http_build_query($query, '', '&', PHP_QUERY_RFC3986);
        }

        return $url;
    }
}
