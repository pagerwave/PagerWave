<?php

declare(strict_types=1);

namespace PagerWave\UrlGenerator;

use PagerWave\Exception\UrlGenerationFailedException;

interface UrlGeneratorInterface
{
    /**
     * @param string $group  the key of the array in which to store the params
     * @param array  $params map of field names to stringable values
     *
     * @throws UrlGenerationFailedException if URL generation failed
     */
    public function generateUrl(string $group, array $params): string;
}
