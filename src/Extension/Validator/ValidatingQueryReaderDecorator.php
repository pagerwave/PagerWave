<?php

declare(strict_types=1);

namespace PagerWave\Extension\Validator;

use PagerWave\DefinitionInterface;
use PagerWave\Exception\QueryValidationFailedException;
use PagerWave\QueryInterface;
use PagerWave\QueryReader\QueryReaderInterface;

/**
 * Adds rudimentary validation capabilities to a query reader.
 */
final class ValidatingQueryReaderDecorator implements QueryReaderInterface
{
    /**
     * @var QueryReaderInterface
     */
    private $decorated;

    public function __construct(QueryReaderInterface $decorated)
    {
        $this->decorated = $decorated;
    }

    public function getFromRequest(DefinitionInterface $definition): QueryInterface
    {
        $query = $this->decorated->getFromRequest($definition);

        if (
            $definition instanceof ValidatingDefinitionInterface &&
            $query->isFilled()
        ) {
            foreach ($definition->getFieldNames() as $name) {
                if (!$definition->isFieldValid($name, $query->get($name))) {
                    throw QueryValidationFailedException::create($name);
                }
            }
        }

        return $query;
    }
}
