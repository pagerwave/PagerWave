<?php

declare(strict_types=1);

namespace PagerWave\Extension\Validator;

interface ValidatingDefinitionInterface
{
    /**
     * @param string|mixed $value
     */
    public function isFieldValid(string $fieldName, $value): bool;
}
