<?php

declare(strict_types=1);

namespace PagerWave\Extension\DateTime;

use PagerWave\EntryReader\EntryReaderInterface;

/**
 * Decorator for entry readers that transforms DateTime objects into ISO-8601
 * strings.
 */
final class DateTimeEntryReaderDecorator implements EntryReaderInterface
{
    /**
     * @var EntryReaderInterface
     */
    private $decorated;

    public function __construct(EntryReaderInterface $decorated)
    {
        $this->decorated = $decorated;
    }

    public function read($entry, string $fieldName)
    {
        $value = $this->decorated->read($entry, $fieldName);

        if (!$value instanceof \DateTimeInterface) {
            return $value;
        }

        return $value->format(\DateTimeInterface::ATOM);
    }
}
